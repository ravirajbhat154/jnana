#!flask/bin/python
from flask import Flask

app = Flask(__name__)

class GuruAI():

	def __init__(self):
		print ("Initial")
	
	@app.route('/')
	def index():
		return "Hello, World!"

	def app_start(self):
		app.run(debug=True,host="0.0.0.0")	
	
if __name__ == "__main__":
	guru_ai = GuruAI()
	guru_ai.app_start()