
class DocumentSimilarity():
    #tool: spacy
    def __init__(self,tool='spacy'):

        if tool=='spacy':
            from core.similarity.spacy_similarity import SpacySimilarity
            self.core_similarity_service = SpacySimilarity()
        elif tool=='guruai':
            print ("Hi")


    def getSimilarity(self,source_document,destination_document):
        return self.core_similarity_service.getSimilarity(source_document,destination_document)

if __name__ == "__main__":
    document_similarity = DocumentSimilarity()
    txt1="C# (\/si: ʃɑːrp\/) is a multi-paradigm programming language encompassing strong typing, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines. It was developed around 2000 by Microsoft within its .NET initiative and later approved as a standard by Ecma (ECMA-334) and ISO (ISO\/IEC 23270:2006). C# is one of the programming languages designed for the Common Language Infrastructure.C# is a general-purpose, object-oriented programming language. Its development team is led by Anders Hejlsberg. The most recent version is C# 7.3, which was released in 2018 alongside Visual Studio 2017 version 15.7.2."
    txt2="A programming language is a formal language which comprises a set of instructions used to produce various kinds of output. Programming languages are used to create programs that implement specific algorithms. Most programming languages consist of instructions for computers, although there are programmable machines that use a limited set of specific instructions, rather than the general programming languages of modern computers. Early ones preceded the invention of the digital computer, the first probably being the automatic flute player described in the 9th century by the brothers Musa in Baghdad, during the Islamic Golden Age. From the early 1800s, programs were used to direct the behavior of machines such as Jacquard looms, music boxes and player pianos. However, their programs (such as a player piano's scrolls) could not produce different behavior in response to some input or condition.Thousands of different programming languages have been created, mainly in the computer field, and many more still are being created every year. Many programming languages require computation to be specified in an imperative form (i.e., as a sequence of operations to perform) while other languages use other forms of program specification such as the declarative form (i.e. the desired result is specified, not how to achieve it).The description of a programming language is usually split into the two components of syntax (form) and semantics (meaning). Some languages are defined by a specification document (for example, the C programming language is specified by an ISO Standard) while other languages (such as Perl) have a dominant implementation that is treated as a reference. Some languages have both, with the basic language defined by a standard and extensions taken from the dominant implementation being common."
    print (document_similarity.getSimilarity(txt1,txt2))