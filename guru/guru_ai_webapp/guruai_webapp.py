from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template , jsonify
app = Flask(__name__)

import sys, os
sys.path.insert(0, os.path.abspath('..'))
from utils.mongo_con import MongoCon
from utils.config_parser import ConfigParser
import re

mongo_client= MongoCon.get_connection()
db=mongo_client[ConfigParser.get('DB_NAME')]
entity_coll = db[ConfigParser.get('ENTITY_COLL_NAME')]

@app.route('/')
def home():
    return sampleActionMethod()


@app.route('/sampleActionMethod', methods=['GET', 'POST'])
def sampleActionMethod():
    if request.method == 'POST':
        station_name=request.form.get('autocomplete')
        if station_name.strip()!='':
            query = {'redirects':{'$regex':re.compile('^'+station_name,re.IGNORECASE)}}
            mongo_obj = entity_coll.find_one(query)
            return render_template('sampleTemplate.html',pagestatus1="active",mongo_obj=mongo_obj)
        else:
            return render_template('sampleTemplate.html',pagestatus1="active")
    else:
        return render_template('sampleTemplate.html',pagestatus1="active")

@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    query = {'redirects':{'$regex':re.compile('^'+request.args.get('q'),re.IGNORECASE)}}
    display= { "_id" :False}
    results=[]
    for station_obj in entity_coll.find(query,display):
        for station in station_obj['redirects']:
            matches = re.match(re.compile('^'+request.args.get('q'),re.IGNORECASE), station)
            if matches:
                results.append(station)
    return jsonify(matching_results=results)
		
if __name__ == '__main__':
     app.run(host='0.0.0.0', port=5000, debug=True)

