from utils.config_parser import ConfigParser
from entity.wiki_entity_harvester import WikiEntityHarverster

class Guru():

	def __init__(self):
		self.entity_name=ConfigParser.get('ENTITY_NAME')
		self.wiki_entity_harverster=WikiEntityHarverster(self.entity_name)
		
	def start_entity_harvesting(self):
		
		print ("Loading Seed data for "+ self.entity_name)
		self.wiki_entity_harverster.load_seed_data()
		print (self.entity_name +" Seed data Uploaded" )
		
		print ("Started harversting L1 Seed data for "+ self.entity_name)
		self.wiki_entity_harverster.get_entities_by_seed_data()
		print ("Compleated harversting L1 Seed data for "+ self.entity_name)
		
		print ("Started harversting Additional Data for L1 Seed data for "+ self.entity_name)
		self.wiki_entity_harverster.get_additional_details()
		print ("Compleated harversting Additional Data for L1 Seed data for "+ self.entity_name)
		
if __name__ == "__main__":

	guru = Guru()
	guru.start_entity_harvesting()