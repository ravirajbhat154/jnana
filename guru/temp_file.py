import requests
import json
import re
page_title="Amgen"
url="https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&format=json&titles="+page_title+"&rvsection=0"

resp=requests.get(url)
resp=resp.json().get("query")
resp=resp.get("pages")

info_box={}
for key ,value in resp.items():
    if "revisions" in value.keys():
        resp=value.get("revisions")[0]["*"]
        search_res=re.findall(r'(?=\{Infobox)(\{([^{}]|([{}]))*\}\}\n\n)',resp,re.M)
        search_res=search_res[0][0]
        info_arr=search_res.split("\n|")
        for attr_str in info_arr:
            if "=" in attr_str:
                index=attr_str.find('=')
                attr=attr_str[:index].strip("=").strip()
                value=attr_str[index+1:]
                if "}}\n}}" in value:
                    value=value.replace("}}\n}}","}}")
                value=value.strip()
                info_box[attr]=value     
        break

print(info_box)
#(?=\{Infobox)(\{([^{}]|(\1)|([{}]))*\}\}\n\n)