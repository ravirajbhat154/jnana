import configparser
import os

class ConfigParser():
	config = configparser.ConfigParser()
	path = os.path.abspath(__file__)
	dir_path = os.path.dirname(path)
	config.read(dir_path+'/../config.ini')

	@staticmethod
	def get(key):
		return ConfigParser.config['DEFAULT'][key]
	
if __name__ == "__main__":
	print( ConfigParser.get('ENTITY_NAME') )