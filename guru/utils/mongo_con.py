from pymongo import MongoClient
from utils.config_parser import ConfigParser

class MongoCon(object):
    __db = None

    @classmethod
    def get_connection(cls,ip= ConfigParser.get('MONGO_SERVER'),port=int(ConfigParser.get('MONGO_PORT'))):
        if cls.__db is None:
            cls.__db = MongoClient(ip,port)
        return cls.__db