import sys, os
sys.path.insert(0, os.path.abspath('..'))

from utils.mongo_con import MongoCon
from utils.config_parser import ConfigParser
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile

class ExportEntityToExcel:
	def __init__(self):
		self.mongo_client= MongoCon.get_connection()		
		self.db=self.mongo_client[ConfigParser.get('DB_NAME')]
		
	def get_entity_data_to_excel(self,entity):
		print('Writing '+ entity +' data to Excel')
		entity_data=list(self.db[entity].find({}, {'_id': False}))
		seed_data=list(self.db[entity+'_seed_data'].find({}, {'_id': False}))
		writer = ExcelWriter(entity+'.xlsx')
		df = pd.DataFrame.from_dict(seed_data)
		df.to_excel(writer,entity+'_seed_data',index=False)
		df = pd.DataFrame.from_dict(entity_data)
		df.to_excel(writer,entity,index=False)
		writer.save()
		print('Completed '+ entity +' data to Excel')

if __name__ == "__main__":
	
	arg1 = sys.argv[1]
	isentityexist=0
	for x in os.listdir('./../entity/en'):
		if(x == arg1):
			isentityexist=1
			
	if(isentityexist==1):	
		export_entity_to_excel = ExportEntityToExcel()
		export_entity_to_excel.get_entity_data_to_excel(arg1)
	else:
		print(arg1 +' Entity Doesnot exist')
	
	
		
		