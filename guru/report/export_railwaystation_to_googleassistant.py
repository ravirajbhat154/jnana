import sys, os
sys.path.insert(0, os.path.abspath('..'))

from utils.mongo_con import MongoCon
from utils.config_parser import ConfigParser
import json


class ExportTOGoogleAssistant:
	def __init__(self):
		self.mongo_client= MongoCon.get_connection()		
		self.db=self.mongo_client[ConfigParser.get('DB_NAME')]
		
	def export_to_googleassistant(self):
		print('Exporting railwaystations data to Json')
		entity_data=list(self.db['railwaystations'].find({}, {'_id': False}))
		
		entity_list=[]
		for entity in entity_data:
			if 'station_code' in entity.keys() and entity['station_code'] is not None :

				rediredcts=list(entity['redirects'])
				if entity['station_code'] not in rediredcts:
					rediredcts.append(entity['station_code'])
				staion_obj={}
				staion_obj['value']=str(entity['station_code'])
				staion_obj['synonyms']= rediredcts
				entity_list.append(staion_obj)
				
		with open('railwaystations.json', 'w') as outfile:
			json.dump(entity_list, outfile)
		
		print("Exported "+str(len(entity_list))+" railwaystations data to Json")

if __name__ == "__main__":

	export_to_google_assistant = ExportTOGoogleAssistant()
	export_to_google_assistant.export_to_googleassistant()

	
	
		
		