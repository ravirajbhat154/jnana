#responsible for harvest data from Wiki 
import wikipedia
import requests

common_files= set({'File:Folder Hexagonal Icon.svg', 'File:Blue pencil.svg', 'File:Portal-puzzle.svg','File:Commons-logo.svg',
'File:Wikisource-logo.svg','File:Semi-protection-shackle.svg','File:Wiktionary-logo-v2.svg','File:Wikivoyage-Logo-v3-icon.svg',
'File:Wikiquote-logo.svg','File:Speaker Icon.svg','File:Loudspeaker.svg','File:Symbol book class2.svg','File:Wikibooks-logo.svg',
'File:Wikiversity-logo-en.svg','File:Open book nae 02.svg','File:Wikiversity-logo-Snorky.svg','File:Symbol support vote.svg','File:Wikidata-logo.svg','File:Cscr-featured.svg','File:Increase2.svg','File:Openstreetmap_logo.svg','File:Wikipedia-logo-v2.svg','File:Pending-protection-shackle.svg'})

rejected_pagetitle_prefixes=('List of','Template:',)

class WikiSpider:

	def __init__(self):
		self.base_url="https://en.wikipedia.org/w/api.php"
		

	def get_page_details(self,page_title):
		page={}
		try:
			wiki_page = wikipedia.WikipediaPage(page_title)
			page= {
			'title':wiki_page.title.strip(),
			'url':wiki_page.url.strip(),
			'summary':wiki_page.summary.strip(),
			'content':wiki_page.content.strip(),
			'categories':wiki_page.categories
			}
		except wikipedia.exceptions.WikipediaException as err:
			print('WikipediaException error:', err)
		return page
		
	def search_by_pagetitle(self,query,results=10,suggestion=False):
		try:
			pages = wikipedia.search(query, results, suggestion)
		
		except wikipedia.exceptions.WikipediaException as err:
			print('WikipediaException error:', err)	

		return pages

	#https://en.wikipedia.org//w/api.php?action=query&format=json&prop=redirects&titles=computer%20aided%20design&redirects=1&converttitles=1&rdnamespace=0&rdlimit=500	
	def get_redirects_list(self,page_title):
		#Namespace=(Article)
		params= {
            'action':'query',
			'prop':'redirects',
            'format':'json',
            'rdnamespace':'0',
			'redirects':'1',
			'converttitles':'1',
            'rdlimit':'500',
            'titles':page_title
		}
	
		response = requests.get(self.base_url,params=params)
		
		json_response= response.json().get("query")
		redirects_list = []

		pages=json_response.get("pages")
		for key, value in pages.items():
			if int(key) > 0:
				if 'redirects' in value.keys():
					for redirects in value.get("redirects"):
						redirects_list.append(redirects.get("title"))

		redirects=None
		if 'redirects' in json_response.keys():
			redirects=json_response.get('redirects')[0]

		normalized=None
		if 'normalized' in json_response.keys():
			normalized=json_response.get('normalized')[0]
			
		if page_title not in redirects_list:
			redirects_list.append(page_title)
			
		result={
			'redirects':redirects_list,
			'query_redirects_to':redirects,
			'query_normalized_to': normalized
		}
		return result
		
	#https://en.wikipedia.org/w/api.php?action=query&format=json&list=categorymembers&redirects=1&cmtitle=Category%3AComputer-aided+design&cmlimit=500&cmnamespace=0	
	def get_pagelist_bycategory(self,category_title_name):
		#Namespace=(Article)
		params= {
            'action':'query',
			'list':'categorymembers',
            'format':'json',
            'cmnamespace':'0',
			'redirects':'1',
            'cmlimit':'500',
            'cmtitle':'Category:'+category_title_name
		}
		
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		categorymembers=json_response.get("categorymembers")
		page_list=[]
		for categorymember in categorymembers:
			if not categorymember.get("title").startswith(rejected_pagetitle_prefixes):
				page_list.append(categorymember.get("title"))
			
		return 	page_list
		
	#https://en.wikipedia.org/w/api.php?action=query&format=json&prop=langlinks|langlinkscount&titles=Albert_Einstein&llprop=url%7Cautonym%7Clangname&lllimit=500
	def get_all_language_details(self,page_title):
		params={
			'action':'query',
			'format':'json',
			'prop':'langlinks|langlinkscount',
			'titles':page_title,
			'llprop':'url|autonym|langname',
			'lllimit':'500'
		}
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		pages=json_response.get("pages")
		langlinks ={}
		for key, value in pages.items():
			if int(key) > 0:
				if 'langlinks' in value.keys():
					langlinks=value.get("langlinks")

		return langlinks
	
	#https://en.wikipedia.org//w/api.php?action=query&format=json&prop=images&titles=Albert_Einstein&imlimit=500
	#https://en.wikipedia.org//w/api.php?action=query&format=json&prop=pageimages&titles=Albert%20Einstein&piprop=name%7Coriginal&pilimit=50&pilicense=free
	
	#https://en.wikipedia.org//w/api.php?action=query&format=json&prop=imageinfo&titles=File%3AAlbert%20Einstein%20Head.jpg&iiprop=timestamp%7Cuser%7Curl
	def get_files(self,page_title):
	    #pageimage
		params={
			'action':'query',
			'format':'json',
			'prop':'pageimages',
			'piprop':'name|original',
			'pilicense':'free',
			'titles':page_title,
			'pilimit':'50'
		}
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		pages=json_response.get("pages")
		pageimage={}
		for key, value in pages.items():
			pageimage['file_title']=value.get("pageimage")
			if 'original' in value.keys():
				pageimage['url']=value.get("original").get('source')

		#all files
		params={
			'action':'query',
			'format':'json',
			'prop':'images',
			'titles':page_title,
			'imlimit':'500'
		}
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		pages=json_response.get("pages")
		
		files ={}
		for key, value in pages.items():
			if int(key) > 0:
				if 'images' in value.keys():
					files=value.get("images")
					
		all_files_set=set()
		for file in files:
			if file['title'] not in common_files:
				all_files_set.add(file['title'])

		#get url
		#https://en.wikipedia.org//w/api.php?action=query&format=json&prop=imageinfo&titles=File:Citizen-Einstein.jpg|File:Albert Einstein german.ogg&iiprop=timestamp%7Cuser%7Curl	
		#print (list(all_files_set))
		#print ('|'.join(list(all_files_set)))
		#TODO split set by 50 mod
		params={
			'action':'query',
			'format':'json',
			'prop':'imageinfo',
			'titles':'|'.join(list(all_files_set)),
			'iiprop':'url',
			'iilimit':'500'
		}
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		pages={}
		if json_response:
			pages=json_response.get("pages")
		
		files_list=[]
		for key, value in pages.items():
			file={}
			file["file_title"]=value.get("title")
			file["url"]=value.get("imageinfo")[0].get("url")
			files_list.append(file)
		
		
		result={'pageimage':pageimage,
				'allfiles':files_list}
		
		return result

	#https://en.wikipedia.org/w/api.php?action=query&format=json&prop=coordinates&titles=Bangalore&redirects=1&colimit=500&coprop=name%7Ctype%7Ccountry%7Cdim%7Cregion%7Cglobe&coprimary=primary
	def get_geocordinates(self,page_title):
		params={
			'action':'query',
			'format':'json',
			'prop':'coordinates',
			'titles': page_title,
			'redirects':'1',
			'colimit':'500',
			'coprop':'name|type|country|dim|region|globe',
			'coprimary':'primary'
		}
		response = requests.get(self.base_url,params=params)
		json_response= response.json().get("query")
		pages={}
		if json_response:
			pages=json_response.get("pages")
		coordinates={}
		for key, value in pages.items():
			if 'coordinates' in value.keys():
				coordinates=value.get("coordinates")[0]
		return coordinates
	
if __name__ == "__main__":

	wiki_spider = WikiSpider()
	#print(wiki_spider.get_page_details("C Sharp (programming language)"))
	#print (wiki_spider.search_by_pagetitle('cad'))
	#print (wiki_spider.get_redirects_list('cad'))
	#print (wiki_spider.get_pagelist_bycategory('Computer-aided design'))
	#print (wiki_spider.get_all_language_details("C Sharp (programming language)"))
	res1 = wiki_spider.get_files("Karnataka")
	print (res1)
	#res2 = wiki_spider.get_files("India")
	#print (res2)
	#print (set(res1).intersection(set(res2)))