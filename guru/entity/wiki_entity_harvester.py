# Path hack.
import sys, os
sys.path.insert(0, os.path.abspath('..'))
from utils.mongo_con import MongoCon
from utils.config_parser import ConfigParser
from wiki_spider.wiki_spider import WikiSpider

import pandas as pd
import codecs
import re

class WikiEntityHarverster:

	def __init__(self,entity_name):
		self.entity_name=entity_name
		self.base_url="https://en.wikipedia.org/w/api.php"
		self.culture=ConfigParser.get('CULTURE')
		self.seed_data_path=os.path.dirname(os.path.abspath(__file__))+'/'+self.culture+'/'+entity_name+'/seed_data.xls'
		self.mongo_client= MongoCon.get_connection()
		
		self.db=self.mongo_client[ConfigParser.get('DB_NAME')]
		self.entity_coll = self.db[ConfigParser.get('ENTITY_COLL_NAME')]
		self.entity_seed_coll =self.db[ConfigParser.get('ENTITY_COLL_NAME')+'_seed_data']
		
		self.wiki_spider= WikiSpider()

	def load_seed_data(self):
		seed_data = pd.read_excel(self.seed_data_path,sheet_name='seed_data')
		inserted_count=0
		for index, row in seed_data.iterrows():
			skill_obj=row.to_dict()
			skill_obj['seed_categories']= list(filter(None, skill_obj['seed_categories'].split(',')))
			db_content = self.entity_seed_coll.find_one({"title": skill_obj["title"] })
			if not db_content:
				wikidata=self.wiki_spider.get_page_details(skill_obj["title"])
				seed_data = skill_obj.copy()
				seed_data.update(wikidata)
				self.entity_seed_coll.save(seed_data)
				inserted_count=inserted_count+1
			
		print ("Successfully loaded skill seed data, Total Count :  "+str(inserted_count) )

	def get_entities_by_seed_data(self):
		seed_datas = self.entity_seed_coll.find()
		for seed_data in seed_datas:
			for seed_categorie in seed_data["seed_categories"]:
				page_list=self.wiki_spider.get_pagelist_bycategory(seed_categorie)
				for pagetitle in page_list:
					
					db_content = self.entity_coll.find_one({"title": pagetitle })
					if db_content:
						wiki_page=db_content
						if 'seed_categories' in db_content.keys() and seed_categorie not in db_content['seed_categories']:
							seed_cat=db_content['seed_categories']
							seed_cat.append(seed_categorie)
							wiki_page['seed_categories']=[seed_cat]
						else:
							if 'seed_categories' in db_content.keys():
								wiki_page['seed_categories']=list(set(db_content['seed_categories']))
							else:
								wiki_page['seed_categories']=[seed_categorie]
					else:
						wiki_page=self.wiki_spider.get_page_details(pagetitle)
						wiki_page['seed_categories']=[seed_categorie]	
					self.entity_coll.save(wiki_page)
					print ("Saved seed_categorie :"+ seed_categorie +" pagetitle :  "+pagetitle )
					
	def get_additional_details(self):
		#read all the wikipages that not containing additional fields like redirects,laglinks ,files etc
		entites = self.entity_coll.find({"redirects": {"$exists": False}})
		
		#for each document and apply following
		for entity in entites:
			
			#redirects
			redirects_result=self.wiki_spider.get_redirects_list(entity['title'])
			entity['redirects']=redirects_result['redirects']
			entity['query_redirects_to']=redirects_result['query_redirects_to']
			entity['query_normalized_to']=redirects_result['query_normalized_to']
			
			#langlinks
			langlinks_result=self.wiki_spider.get_all_language_details(entity['title'])
			entity['langlinks']=langlinks_result
			
			#files
			files_result=self.wiki_spider.get_files(entity['title'])
			entity['pageimage']=files_result['pageimage']
			entity['allfiles']=files_result['allfiles']
			
			#geocordinates
			geocordinates_result=self.wiki_spider.get_geocordinates(entity['title'])
			entity['geocordinates']=geocordinates_result
			##Compute entity spesific fields
			if self.entity_name=='railwaystations':
				entity['station_code']=self.get_station_code(entity['summary'])
				
			self.entity_coll.save(entity)
			

			print ("Computed additional data for :" + entity['title'])
	
	def get_station_code(self,summary_txt):
		station_code =re.findall(r'([A-Z][A-Z]+|[A-Z][A-Z]+[/][A-Z]|[A-Z][A-Z]+[/][A-Z][A-Z]+)',summary_txt)
		
		if (len(station_code)!=0):
			return station_code[0]
		else:
			return None

if __name__ == "__main__":
	wiki_entity_harverster = WikiEntityHarverster('skill')
	#wiki_entity_harverster.load_seed_data()
	wiki_entity_harverster.get_entities_by_seed_data()